import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {

    private static final String INPUT_FILE = "input.txt";
    private static final String OUTPUT_FILE = "output.txt";
    private static final String COMPANY_NAME = "onetwotrip";
    private static final String OUT_FORMAT = "%s - (%d, %d);%n";
    private static final String IMPOSSIBLE = "Impossible\n";
    private static final String FILE_NOT_FOUND = "File not found";

    public static void main(String[] args) throws IOException {
        char[][] chars = inputCharsMatrix();
        List<String> result;
        if (chars != null) {
            result = findChars(chars.length, chars[0].length, chars);
            outputToFile(result);
        }
    }

    private static char[][] inputCharsMatrix() {
        File input = new File(INPUT_FILE);
        try {
            Scanner scanner = new Scanner(input);
            String inp = scanner.nextLine();
            int M = Integer.parseInt(inp.split(" ")[0]);
            int N = Integer.parseInt(inp.split(" ")[1]);
            char[][] chars = new char[M][N];
            for (int i = 0; i < M; i++) {
                chars[i] = scanner.nextLine().toCharArray();
            }
            scanner.close();
            return chars;
        } catch (FileNotFoundException e) {
            System.out.println(FILE_NOT_FOUND);
            return null;
        }
    }

    private static void outputToFile(List<String> result) {
        try {
            File output = new File(OUTPUT_FILE);
            BufferedWriter writer = new BufferedWriter(new FileWriter(output));
            if (!result.isEmpty()) {
                for (String s : result) {
                    writer.write(s);
                }
            } else {
                writer.write(IMPOSSIBLE);
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<String> findChars(int m, int n, char[][] chars) {
        List<String> result = new ArrayList<>();
        boolean[][] used = new boolean[m][n];

        for (int i = 0; i < COMPANY_NAME.length(); i++) {
            char current = COMPANY_NAME.toCharArray()[i];

            boolean notFound = true;
            for (int r = 0; r < m && notFound; r++) {
                for (int c = 0; c < n && notFound; c++) {
                    char find = chars[r][c];
                    if (!used[r][c] && Character.toLowerCase(find) == current) {
                        used[r][c] = true;
                        notFound = false;
                        result.add(String.format(OUT_FORMAT, find, r, c));
                    }
                }
            }
            if (notFound) {
                result.clear();
                break;
            }
        }
        return result;
    }
}